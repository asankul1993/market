from rest_framework import serializers
from . import models

class ShopSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Category
        fields = ('name', 'slug')