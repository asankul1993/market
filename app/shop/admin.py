from django.contrib import admin
from .models import Category, Product
# Модель категории
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name', )}
# Модель товара
class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'price', 'stock', 'available', 'size', 'color', 'created', 'updated']
    list_filter = ['available', 'size', 'color', 'created', 'updated']
    list_editable = ['price', 'size', 'color', 'stock', 'available']
    prepopulated_fields = {'slug': ('name', )}
admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)