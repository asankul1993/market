from django.shortcuts import render, get_object_or_404
from .models import Category, Product
from rest_framework import generics
from . import models
from . import serializers

class CategoryListView(generics.ListCreateAPIView):
    queryset = models.Category.objects.all()
    serializer_class = serializers.ShopSerializer
# Страница с товарами
# def base_view(request):
#     categories = Category.objects.all()
#     products = Product.objects.all()
#     context = {
#         'categories' : categories,
#         'products': products,
#     }
#
#     return render(request, 'base.html', context)