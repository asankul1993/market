FROM python:3.6-alpine
MAINTAINER ASANKUL

ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt /requirements.txt
RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev sudo libjpeg-turbo-dev libpng-dev


RUN pip install -r /requirements.txt

RUN mkdir /app
WORKDIR /app/app
COPY ./app /app

RUN adduser -D user
USER user

